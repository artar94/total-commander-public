'=====================================================================
' �������� ��������� ��������� � ������ �������� ������� � ���

' � ���������� ������ �� TC ������ ���� ���������:
' %L "%T"
'=====================================================================
Option Explicit
Dim FSO, WSH, OTF, Target, NewTar
Dim Selected
Set FSO = CreateObject("Scripting.FileSystemObject")
Set WSH = CreateObject("WScript.Shell")
Set OTF = FSO.OpenTextFile(WScript.Arguments(0), 1)
Target  = WScript.Arguments(1)

MsgBox("�������� ������ ��������� ����� � ����������� �� ��������� ��������� ���������")
  
Do While Not OTF.AtEndOfStream
  Selected = OTF.ReadLine
  If FSO.FileExists(Selected) Then
    WSH.Run "fsutil file createnew """ & Target & FSO.GetFile(Selected).Name & """ 0", 7, True
  End If
  If FSO.FolderExists(Selected) Then
    NewTar = Target & FSO.GetFolder(Selected).Name
    If Not FSO.FolderExists(NewTar) Then
      FSO.CreateFolder(NewTar)
    End If
    FolderProcess FSO.GetFolder(Selected), NewTar & "\"
  End If
Loop
MsgBox("���������!")
Set OTF = Nothing
Set FSO = Nothing
Set WSH = Nothing
WScript.Quit()

Function FolderProcess(Fold, Tar)
  Dim sf, f, NewF
  For Each sf in Fold.SubFolders
    NewF = Tar & sf.Name
    If Not FSO.FolderExists(NewF) Then
      FSO.CreateFolder(NewF)
    End If
    FolderProcess sf, NewF & "\"
  Next
  For Each f in Fold.Files
    WSH.Run "fsutil file createnew """ & Tar & FSO.GetFile(f).Name & """ 0", 7, True
  Next
End Function