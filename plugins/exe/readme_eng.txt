Tweak Total Commander
Version 10 build 050915 (zombie)

Changes since v.6.0.3.3 (2006) :

1) Removed old settings that already exist in internal TC dialogues;
2) Old settings were replaced with new ones;
3) The code, that stops the program on �64 systems, was removed;
4) Added two new drive icons (available since TC v.8.50).
   Default drive icons were changed;
5) Fixed some weird visual effects on some buttons;
6) Contact information and web links were edited;
7) EXE file was compiled with no compression (VirusTotal result is 0/56).

--
WBR,
Shura
