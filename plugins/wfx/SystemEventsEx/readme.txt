This software is provided "as is" and is without warranty of any kind.
This plugin is freeware.

"System Events Ex" - FS-plugin for Total Commander that allows you to view 
all logs of event  Windows. Created by analogy with the plugin Events NT 1.3 
and can serve as a substitute.

System Requirements:
---------------------
- Windows 2003/Windows XP and later
- Total Commander x32/x64

Features:
---------
- Select interface language (English, Russian, Polish and Hungarian included) 
  in the dialog of configuration plugin.
- The log properties dialog box allows you to:
  - view information about the event log;
  - saves the event log to a backup file;
  - clears the event log, and optionally saves the current copy of the event log 
    to a backup file;
  - change the setting of automatic cleaning / overwriting events. 
- View detailed information about the event at Lister (F3, Ctrl+Q) or in 
  the event viewer dialog box, that can be called by pressing the Enter key 
  or by double-clicking on the event.
- Copy the selected events as a list of files.

Total Commander in the events list contains the following information:
a) when you use the proposed default extended set of columns: 
  - Name (number record the event and the name of the event source);
  - Ext (type of event: Information, Warning, Error, Success Audit, Failure Audit);
  - Date (time the event was created);                      
  - Account; 
  - Category;
  - Code (ID);
  - Description;

b) when you use the standard detailed set of columns (Ctrl+F2):
  - Name - number record the event and the name of the event source;
  - Ext - type of event (Information, Warning, Error, Success Audit, Failure Audit);
  - Date - time the event was created;                      
  - Size - identifies the event.
  
Restrictions:
------------- 
  - Individual entries cannot be deleted (this is due to Windows).

Tested on Windows XP/Windows Seven.

History:
--------
Version 1.0.3:
  1. Added additional columns 'Account', 'Category', 'Code', 'Description'.
  2. Added the rows with IDs 120-124 in the language localization files, containing 
     the names of additional columns. Check your custom language files. 
  3. Fixed the display of the text description of the events for 
     Windows XP / Windows 2003.
    
Version 1.0.2.a:
  1. Added Hungarian translation of the interface (the author: Bluestar).
  2. Fixed Polish translation of the interface (the author: dmocha).
  No more changes.
  
Version 1.0.2:
  1. Added buttons for navigating through the events in the event viewer dialog 
     box. Buttons allow you to browse previous / next event in the current sort 
     order without closing the dialog. The buttons have shortcuts PageUp and 
     PageDown, respectively.
  2. Added the rows with IDs 62, 204, 205 in the language localization files. 
     Check your custom language files. 

Version 1.0.1:
  1. Added full Unicode support (support of national characters).
  2. Fixed a bug when did not show status icons the events for all of languages 
     except English and Russian.
  3. Added Polish translation of the interface (the author: dmocha).

Version 1.0.0: 
  First version

Author:
-------
Sergey Pidonenko, Ukraine
email: psa1974@fromru.com
