wfx_FileStreams v2.0.5 Beta - ��������� ������ ��� Total Commander.

�������������� ���������� � ��������� - �������� ���������.


������ ��������� �������������/�������������/��������� �������� ������ �� 
NTFS-������.
������������ ������ � ��������� ������� ������ (� �������, � �������� 
�������/��������� ��� � ������������������ ������� ����� "nul" ��� "prn").
����� ������������ ��������� ������� ��������� ������: "rmdir", "del" � "mkdir".


������ ��������� ������������ "��� ����" ("as is"). ����� �� ����������� 
������������ ������ ��������� � �� ���� ������� ��������������� �� ��������� 
����������� ������ ���������. ��������� ���������������� ��������� ("freeware").


������ 2.0 Beta:
 + ������ � x64;
 + ������ � �������� � ���������;
 + �������������� �������;
 + ��������� ������� � TC;
 - ��������������/�����������;
 * ������ �����������.

������ 2.0.1 Beta:
 * ������ ��� �����������/��������� ����� ������ ����� ���������� ���;
 * ������ ����������� � ����������.

������ 2.0.2 Beta:
 * ������ �����������.

������ 2.0.4 Beta:
 * ������ �����������.

������ 2.0.5 Beta:
 * ��������� ������ �������� GDI.


� 2014 - 2018 ProgMan13 (progman13@mail.ru)