wfx_FileStreams v2.0.5 Beta - FS plugin for Total Commander.

Initial developer and sources: Alexeev Alexander.


FS plugin allows to view/edit/create file streams for files on NTFS drives.
Supports working with invalid file names (for example, with leading dots/spaces 
or with reserved names, such as "nul" or "prn"). 
Also supports command-line commands "mkdir", "del" and "rmdir".


These software are provided "as-is".
No warranty provided.
You use this program at your own risk. The author will not response for data 
loss, damages, etc.
while using or misusing this software.


Ver 2.0 Beta:
 + Unicode and 64 bit support;
 + work with streams in directories;
 + information columns;
 + support thumbnails in TC;
 - rename/move;
 * other fixes.

Ver 2.0.1 Beta:
 * for copy/view plugin get now normal file names;
 * some interface fixes.

Ver 2.0.2 Beta:
 * some fixes.

Ver 2.0.4 Beta:
 * some fixes.

Ver 2.0.5 Beta:
 * fixed GDI leak.


� 2014 - 2018 ProgMan13 (progman13@mail.ru)