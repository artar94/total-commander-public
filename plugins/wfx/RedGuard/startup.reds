(***     RedGUARD  autorun template scripts ***)


// Global types

// type VALUE_TYPES = (VAL_INTEGER, VAL_STRING, VAL_EXPANDSTRING, VAL_MULTISZ);

// Global variables - Item types
var HKLM_POL32, HKLM_POL64, HKLM_GPE32, HKLM_GPE64, HKLM_IEEXT32, HKLM_IEEXT64, HKLM_ENV32, HKLM_ENV64, HKCU_ENV32, HKCU_ENV64, HKLM_BE32, HKLM_BE64: TItemType;

// Some constants
const NULL_IT = VAL_INTEGER; 
const NULL_NAME = '';


// Proxy procedures
procedure QTypeInit (value: TItemType);
begin
QInitializeType (value.TypeID,value.RootType,value.EditType,value.ValueType,value.tPlatform,value.DefAddr);
end;

// Find value in any subkey of some key, and add N other values
function  QScanForVals4 (TypeID, ValueName: String; AVT1: VALUE_TYPES; AVN1: String; AVT2: VALUE_TYPES; AVN2: String; AVT3: VALUE_TYPES; AVN3: String; AVT4: VALUE_TYPES; AVN4: String): boolean;
begin
Result := QScanForValues (TypeID, ValueName,4, AVT1,AVN1,AVT2,AVN2,AVT3,AVN3,AVT4,AVN4);
end;

function  QScanForVals3 (TypeID, ValueName: String; AVT1: VALUE_TYPES; AVN1: String; AVT2: VALUE_TYPES; AVN2: String; AVT3: VALUE_TYPES; AVN3: String): boolean;
begin
Result := QScanForValues (TypeID, ValueName,3, AVT1,AVN1,AVT2,AVN2,AVT3,AVN3,NULL_IT,NULL_NAME);
end;

function  QScanForVals2 (TypeID, ValueName: String; AVT1: VALUE_TYPES; AVN1: String; AVT2: VALUE_TYPES; AVN2: String): boolean;
begin
Result := QScanForValues (TypeID, ValueName,2, AVT1,AVN1,AVT2,AVN2,NULL_IT,NULL_NAME,NULL_IT,NULL_NAME);
end;

function  QScanForVals1 (TypeID, ValueName: String; AVT1: VALUE_TYPES; AVN1: String): boolean;
begin
Result := QScanForValues (TypeID, ValueName,1, AVT1,AVN1,NULL_IT,NULL_NAME,NULL_IT,NULL_NAME,NULL_IT,NULL_NAME);
end;


function  QScanForVals (TypeID, ValueName: String): boolean;
begin
Result := QScanForValues (TypeID, ValueName,0, NULL_IT,NULL_NAME,NULL_IT,NULL_NAME,NULL_IT,NULL_NAME,NULL_IT,NULL_NAME);
end;


begin


// Lets run!

(***************** Start of Autorun Policies  *****************)

  HKLM_POL32.TypeID := 'HKLM_POL32'; 
  HKLM_POL32.DefAddr := '\SOFTWARE\Microsoft\Windows\CurrentVersion\policies\Explorer';
  HKLM_POL32.RootType := RT_HKLM;
  HKLM_POL32.EditType := ET_MUST; 
  HKLM_POL32.ValueType := VAL_INTEGER; 
  HKLM_POL32.tPlatform := WOW64_32;
  
  HKLM_POL64 := HKLM_POL32;
  HKLM_POL64.TypeID := 'HKLM_POL64';
  HKLM_POL64.tPlatform := WOW64_64;
  
  QTypeInit(HKLM_POL32);
  QTypeInit(HKLM_POL64);
  
  QAddValue('HKLM_POL32', 'DisableLocalMachineRun');
  QAddValue('HKLM_POL32', 'DisableLocalMachineRunOnce'); 
  QAddValue('HKLM_POL32', 'DisableCurrentUserRun');  
  QAddValue('HKLM_POL32', 'DisableCurrentUserRunOnce');   
  
  QAddValue('HKLM_POL64', 'DisableLocalMachineRun');
  QAddValue('HKLM_POL64', 'DisableLocalMachineRunOnce'); 
  QAddValue('HKLM_POL64', 'DisableCurrentUserRun');  
  QAddValue('HKLM_POL64', 'DisableCurrentUserRunOnce'); 
  
 (*****************  End of Autorun Policies  *****************) 
 
 
 
 (***************** Start of GPE  *****************)

  HKLM_GPE32.TypeID := 'HKLM_GPE32'; 
  HKLM_GPE32.DefAddr := '\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon\GPExtensions';
  HKLM_GPE32.RootType := RT_HKLM;
  HKLM_GPE32.EditType := ET_OPTIONAL; 
  HKLM_GPE32.ValueType := VAL_EXPANDSTRING; 
  HKLM_GPE32.tPlatform := WOW64_32;
  
  HKLM_GPE64 := HKLM_GPE32;
  HKLM_GPE64.TypeID := 'HKLM_GPE64';
  HKLM_GPE64.tPlatform := WOW64_64;
  
  QTypeInit(HKLM_GPE32);
  QTypeInit(HKLM_GPE64);
  
  QScanForVals1('HKLM_GPE32', 'DllName', VAL_EXPANDSTRING, 'DisplayName');
  
  QScanForVals1('HKLM_GPE64', 'DllName', VAL_EXPANDSTRING, 'DisplayName');

  
 (*****************  End of GPE  *****************) 
 


(***************** Start of IE Extensions  *****************)

  HKLM_IEEXT32.TypeID := 'HKLM_IEEXT32'; 
  HKLM_IEEXT32.DefAddr := '\SOFTWARE\Microsoft\Internet Explorer\Extensions';
  HKLM_IEEXT32.RootType := RT_HKLM;
  HKLM_IEEXT32.EditType := ET_OPTIONAL; 
  HKLM_IEEXT32.ValueType := VAL_STRING; 
  HKLM_IEEXT32.tPlatform := WOW64_32;
  
  HKLM_IEEXT64 := HKLM_IEEXT32;
  HKLM_IEEXT64.TypeID := 'HKLM_IEEXT64';
  HKLM_IEEXT64.tPlatform := WOW64_64;
  
  QTypeInit(HKLM_IEEXT32);
  QTypeInit(HKLM_IEEXT64);
  
  QScanForVals4('HKLM_IEEXT64', 'CLSID',  VAL_STRING, 'ClsidExtension', VAL_STRING, 'Icon', VAL_STRING, 'HotIcon', VAL_STRING, 'MenuText');
  
  QScanForVals4('HKLM_IEEXT32', 'CLSID',  VAL_STRING, 'ClsidExtension', VAL_STRING, 'Icon', VAL_STRING, 'HotIcon', VAL_STRING, 'MenuText');

  
 (*****************  End of of IE Extensions  *****************) 

(***************** Start of LM Environment  *****************)

  HKLM_ENV32.TypeID := 'HKLM_ENV32'; 
  HKLM_ENV32.DefAddr := '\SYSTEM\CurrentControlSet\Control\Session Manager\Environment';
  HKLM_ENV32.RootType := RT_HKLM;
  HKLM_ENV32.EditType := ET_OPTIONAL; 
  HKLM_ENV32.ValueType := VAL_EXPANDSTRING; 
  HKLM_ENV32.tPlatform := WOW64_32;
  
  HKLM_ENV64 := HKLM_ENV32;
  HKLM_ENV64.TypeID := 'HKLM_ENV64';
  HKLM_ENV64.tPlatform := WOW64_64;
  
  QTypeInit(HKLM_ENV64);
  QTypeInit(HKLM_ENV32);
  
  QAddAllValues('HKLM_ENV32');
  QAddAllValues('HKLM_ENV64');

  
 (*****************  End of of LM Environment  *****************) 
 
 (***************** Start of User Environment  *****************)

  HKCU_ENV32.TypeID := 'HKCU_ENV32'; 
  HKCU_ENV32.DefAddr := '\Environment';
  HKCU_ENV32.RootType := RT_HKCU;
  HKCU_ENV32.EditType := ET_OPTIONAL; 
  HKCU_ENV32.ValueType := VAL_EXPANDSTRING; 
  HKCU_ENV32.tPlatform := WOW64_32;
  
  HKCU_ENV64 := HKCU_ENV32;
  HKCU_ENV64.TypeID := 'HKCU_ENV64';
  HKCU_ENV64.tPlatform := WOW64_64;
  
  QTypeInit(HKCU_ENV32);
  QTypeInit(HKCU_ENV64);
  
  QAddAllValues('HKCU_ENV32');
  QAddAllValues('HKCU_ENV64');

  
 (*****************  End of of User Environment  *****************) 
 
 (***************** Start of Boot Execute  *****************)

  HKLM_BE32.TypeID := 'HKLM_BE32'; 
  HKLM_BE32.DefAddr := '\SYSTEM\CurrentControlSet\Control\Session Manager';
  HKLM_BE32.RootType := RT_HKLM;
  HKLM_BE32.EditType := ET_MUST; 
  HKLM_BE32.ValueType := VAL_MULTISZ; 
  HKLM_BE32.tPlatform := WOW64_32;
  
  HKLM_BE64 := HKLM_BE32;
  HKLM_BE64.TypeID := 'HKLM_BE64';
  HKLM_BE64.tPlatform := WOW64_64;
  
  QTypeInit(HKLM_BE32);
  QTypeInit(HKLM_BE64);
  
  QAddValue('HKLM_BE32', 'BootExecute');
  QAddValue('HKLM_BE64', 'BootExecute');

  
 (*****************  End of Boot Execute  *****************)  
 
 
 // Set confirmation flags (ConfirmChange = TRUE, ConfirmDelete = TRUE, ConfirmAdd = TRUE, ConfirmRestore = TRUE)


// Guard.SetConfirmation(False, False, False, False);

// Set configuration flags (CanCreateKeys = FALSE,  CanDebugStrings = FALSE)

Guard.SetConfiguration (False, True);

end.