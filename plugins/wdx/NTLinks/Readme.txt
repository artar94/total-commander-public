NTLinks plugin for Total Commander
----------------------------------
(information in English is below)

������ ��� ��������� ���������� � ������ ��������������� NTFS:
+ ��������� �������� ������, ���������� ������� ������ � ����� � ���
+ ��������� ���� �������� ������� ������ � ��������� ���� ��������
+ ��������� ������������� ������, ����������� � Windows Vista/7
+ ������/������ ���� � �������� ������� ��� ������������� ������ � ����� ����������
+ ������������ ��������� ������� ������ (� �������� ������ ����������� �����)
+ ��������� ������� � ������� �����


1. ����� �����������

NTLinks - ��� ���������� ������ ��� ��, ��������������� ��������� �������������� ���� ��� ����������� ���������, ���������������� ������� ��� ������ � ��, ����� ���� ����, ������� ����� ���� ������ � ���� ��������� ���������.

�������������� ����:
	HLNK_Count
		����� ������� ������ (���������, ������� ������ � ������ ����� ��������� �� ����� ���������� ����������);
	HLNK_Index
		������ ����� � �������� ������� (������, �������������� ���������� � �������� ����������� ����� 64-������ ������ �����) - ��� ������ ������� ������ �� ���� ����;
	File_Type
		��� ������� (����, ������� ��� ������ - ��� ��������� ���� ������). ����� ��� ���������� ���� ��, �� ��������� ���������� ������������� ������ �� �����;
	Obj_Type
		��� ������� � ����� ����� ��������������� (����, ������� ��� ����������� ��� ������);
	RP_Target
		������� ������, �� ������� ��������� ����� ��������������� (��� ����� ������������ - ���� ������ ����� ������������ ����� ����, ������ ��� ����� �����). ��� ���� ����� �������� � ���� ���������;
	RP_IsValid
		����������� �������� ������� (���������, �������� �� ���� � �������� �� ��������� ��� ����� ��������������� � ���� � �������);
	Obj_RealPath
		�������� ���� ������ �����/�������� � ������� (������������ ��� ����� ��������������� � ����);
	Obj_RealSize
		�������� ������ ����� (� ������ ������������� ������ �� ����);
	HLNK_Paths
		���� � ������ 10 ������� ������ �����, ���� �������� (��� ��� ������ N-�, ������ � Windows Vista).

��� Windows 2000 ���������� ����� �������������� ��� ����������� ����������� ������ ����� ������������, ���� ��� �� ����� ����� ����� (����� ������ ������ ���� � ������� ����� ������������).

��� �������� ������� � ��������� ���� �������� ��� �������� ����������� - ������� � ����������. � ���������� ������������ ���������� �������������� ���� (���� Volume{GUID}\, ��� GUID - ��������� ������������������ ����������������� ����, ����������� ��������; �� ������ ����� ����������, �������� ������� mountvol � �������), � ����� ������������� ���� ������������� ������ ������������ � �������� ����. � ������� ������������ ������ ����, ���� ��� ������������� ������������� ������. ������ ���� ���������� ������ � Unicode, ���� ������������ �� 7.50 � ������, � ANSI ��� ������ ������.


3. ��������� ���� � �������� �������

����� ���������� ����� ���� � �������� ������� ��� ����� ������������/���������� ��� ������������� ������, ����� ������� ���� ��������� ��������� � ������ ����� �������� � ��������������� ���� ������� ������. ������ �� ��������� ������������� ����� ������� ������������/���������� � �������������� ��������. ��� ���������� �������� �������� ������� � ������ ����� ����� ������������� � ����� ������������/���������� (� �������, ��� ������� �������� ���� ���������� ������ �����). ��� ��������� �������� ���� ������������� ������ ���������� ����� ��������������.

���� ���� ������� ��� �������� \??\ ��� \\?\, ����� ����������� �������������� ��������� � ���������� ������� ��������. ���� � �������� ����� ����� (��������, C:\) ������������� ������������ � ���������� ������������� ����, �������� ����� ������������. ��� �������� ������� ���� � ��������� ����� ����������� ����� ����������. ���� ���� ���������� � ����������� �������������� ����, ��� ����� ��������� ��� �������� (��� ������������� ������ ������� �� ����� ����������, ����� �������� ������������� ���� �� ����� �����).

����� �������� ������� �������� ���� � �������� �������, ���������� �������� ���� ����� � ������ ������ >> ������.


4. ��������� ������ �� ��������

���������� ��������� ����������� ����������, ��������� �� 2 ����� �� ����� ���������� ����� (�������� �� �������� �������), ������� �� �������. ����� ���� ������� ��� ������������� ����� � �������� ��������, � ����� ��� ������� ����� ���������� �� ���� � �� �� ����� � ����� ������. ����� ���������� � ������� ������������ ��������� (������ >> ����� ����� ��������� �� ����������� ������� �������������). ����������� ���� �������������� ��������� ������������ ��� ����� ������.


���������� �� ����������� ������������� ������: http://forum.wincmd.ru/viewtopic.php?t=13191



NTLinks plugin for Total Commander
----------------------------------
(English section)

Plugin allows working with NTFS reparse points data fields:
+ gets file indices and number and paths of hardlinks
+ gets target path for reparse points and real path for all objects
+ supports symbolic links (new in Windows Vista/7)
+ get/set target path for symbolic links and junctions
+ fastest hardlinks comparison (within same volume)
+ Unicode and long paths support


1. General features

NTLinks is a content plugin for TC, which provides some information fields for TC tooltips, custom columns or searching, also there is a field that may be altered in Attributes dialog.

Content fields:
	HLNK_Count
		Number of hard copies (shows how many files in volume folders point to same physical contents);
	HLNK_Index
		Unique volume index of file (string representing unique 64-bit index of file within volume) - allows searching hard copies;
	File_Type
		Object type (file, directory or reparse point - without specific reparse point type). Like TC internal field but detects file symbolic links properly;
	Obj_Type
		Object type with reparse point type (file, directory or detected reparse point type);
	RP_Target
		Target path for reparse point (for mount point - first mount point path for this volume which is a drive letter usually). This field may be set in Attributes dialog;
	RP_IsValid
		Availability of target path (checks if target drive is available and all reparse points in path may be resolved);
	Obj_RealPath
		Real path of any NTFS object (all reparse points are expanded);
	Obj_RealSize
		Real size of a file (in case of symbolic link);
	HLNK_Paths
		Paths of first 10 hard copies of the file, if available (all or N-th one, requires Windows Vista).

If you're running Windows 2000, admin rights are required to correctly resolve first mount point if volume has no letter assigned (current mount point path returned else - it is a valid mount point too).

You may choose between natural and symbolic output mode. In symbolic mode volume GUIDs (look like Volume{GUID}\, where GUID is a hexadecimal string, separated by hyphens; use mountvol console command to get list of volume GUIDs) are displayed as is, also relative targets of symbolic links are left as is. Natural mode displays full path to target, even for relative symbolic links. Each fields return Unicode string for TC 7.50 and newer, and ANSI string for old TC versions.


2. Setting target path

You should open Change Attributes dialog and choose corresponding plugin field to set target path for selected mount point, junction symbolic link. You can't change reparse point type. If you add attribute for empty folder, it is converted into mount point or junction, depending on path (and back, if you clear target path, you get empty folder). You must have admin rights in order to change target path for symbolic links.

If you specify path w/o prefix \??\, additional path preprocessing is performed. Path to drive's root folder (e.g. C:\) is expanded to volume GUID path and you get mount point. If you specify full path to some folder, you get junction. If path is started with volume GUID, you may omit prefix (for symbolic links you must specify prefix to distinguish volume GUID and folder name).

In order to get current target path you should clear edit field and click >> near the field.


3. Compare files by indices

Instrument is able to determine instantly, if two files within the same volume are identical (if they are hard copies), by comparing their file system indices. It may be helpful if you compare folders with hardlinks or if you have junctions to same folders on both sides. You may enable it in user-defined compare dialog (button >> near compare by contents option of Synchronize dialog). Special identical equality sign is displayed for such files.


Discussion page on official board: http://www.ghisler.ch/board/viewtopic.php?t=23681



History:

2025-02-05	1.6.1.292
	* Obj_RealPath field now handles relative links to links
	* RP_IsValid field is recursive now, like Obj_RealPath

2025-01-30	1.6.1.290
	* fixed modifying target attribute for links
	* 'Compare indexes' field renamed to 'Compare indices'

2022-07-04	1.6.1.280
	* fixed hard copy path for links to another volumes
	* fixed setting target attribute for non-links

2019-10-24	1.6.1.270
	+ File_Type field added (like TC internal field but treats file symlinks as reparse points)
	* another relative symlink real path bug
	* some optimizations

2018-09-01	1.6.0.254
	* ignore last file cache on timestamp change
	* fixed symlink target for U: drive
	* fixed relative symlink real path when one of parents is a link
	* always show relative target for relative symlinks
	* if both symbolic and natural targets were requested for an UNC path, second one was broken

2016-01-07	1.6.0.244
	* reading relative not null-terminated symlink paths

2015-10-26	1.6.0.242
	+ long path support

2012-03-12	1.5.3.216
	+ shows size 1 KB/MB/GB instead of 0 for non-empty files

2012-04-26	1.5.3.214
	+ new field to get paths of hard copies of the file (first 10 items, requires Windows Vista)
	+ new field to get real size of a file behind symlink
	* wrong real path for file symlinks
	* full target was shown instead of relative one sometimes
	* improved junction detection if path starts with volume GUID
	* ANSI to Unicode conversion bug in non-Unicode functions (introduced in 1.5.0.108)

2011-09-19	1.5.2.162
	+ 64-bit version of plugin added (requires 64-bit TC version)
	* wrong reparse point type detection after creating reparse point

2011-06-15	1.5.1.156
	+ special identical equality icon for index compare (for Synchronize by contents dialog)
	+ removes \??\ prefix for symbolic target of mount points too
	+ allows setting path starting with volume GUID w/o prefix \??\ (for mount points and junctions)
	+ understands symlinks and junctions starting with volume GUID
	+ gets privilege required to modify symlinks
	* clears cache on refresh or change dir in TC
	* wrong prefix for target of absolute symlink
	* shows number of hardlinks for dirs and symlinks

2011-06-11	1.5.0.124
	+ keeps last file data in cache (should work faster when many plugin columns used)
	+ natural mode for symlinks now returns full path even if link contains relative one
	+ set/remove target path feature (via Attributes dialog) for mount points/junctions and symbolic links
	+ compare files by file/volume index function (for Synchronize by contents dialog)
	* max number of expands in real path reduced to 32 (Windows doesn't allow more than 31 reparse points in a path)
	* faster algorithm for real path
	* fixed some reparse point types
	* Unicode and ANSI fields joined together (same fields return ANSI strings in old TC versions)

2010-05-12	1.0.0.74
	* returned wrong real path for symlinks with target started with backslash (relative to root) due to Unicode conversion

2010-04-01	1.0.0.72
	+ Unicode support
	+ two new Unicode fields added: RP_TargetW, Obj_RealPathW
	+ changed module base address
	* for non-existent files type 'reparse point' was displayed

2009-09-01	1.0.0.56
	+ uses FindFirstVolumeMountPoint function under Windows 2000 if available (admin rights)
	+ symbolic mode for target and real path fields - doesn't expand mount points

2009-08-29	1.0.0.48
	+ real path field that have reparse points expanded
	+ max 256 expands to avoid loops
	* reparse points not always expanded if current program folder is reparse point
	* next reparse point may leave unexpanded if its nesting level less than processed level

2009-08-28	1.0.0.30
	+ some new reparse point types
	* now supports Windows 2000 (calls GetVolumePathNamesForVolumeNameW dynamically)

2009-08-26	1.0.0.xx
	! first public release
	+ returns info about file index, number of copies, reparse points type and target, target availability
	+ symlinks support
