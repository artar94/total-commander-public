xz plugin v1.0 for Total Commander

Automatic installation
==================
Double click on the plugin archive in Total Commander,
then follow the instructions

Manual installation
================
1. Unzip the xz.wcx and xz.wcx64 to the Total Commander directory
2. In Total Commander, choose Configuration - Options
3. Open the 'Plugins' page
4. Click on "Configure" in the packer plugin section
5. type  xz  as the extension
6. Click 'new type', and select the xz.wcx
7. Click OK

What it does:

  This plugin allows you to create and extract xz and tar.xz archives.

Christian Ghisler
http://www.ghisler.com

History:
2012.05.31 initial release
