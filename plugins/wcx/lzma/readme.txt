lzma plugin v2.1 for Total Commander

Automatic installation
==================
Double click on the plugin archive in Total Commander,
then follow the instructions

Manual installation
================
1. Unzip the lzma.wcx and lzma.wcx64 to the Total Commander directory
2. In Total Commander, choose Configuration - Options
3. Open the 'Plugins' page
4. Click on "Configure" in the packer plugin section
5. type  lzma  as the extension
6. Click 'new type', and select the lzma.wcx
7. Click OK

What it does:

  This plugin allows you to create and extract lzma and tar.lzma archives.

Christian Ghisler
http://www.ghisler.com

History:
2012.06.20 Re-Release version 2.1 with digitally signed dlls
2012.06.18 Release version 2.1
2012.06.18 Fixed: Settings dialog missing in 64-bit version
2012.06.11 Release version 2.0
2012.06.10 Added: Pack function
2012.05.31 initial release
