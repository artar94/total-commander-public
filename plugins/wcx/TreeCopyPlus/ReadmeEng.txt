TreeCopyPlus 1.051
(c) Alexander Golikov aka SUKER
----------------
Packer plugin for Total Commander. It's intended for copying/moving files with keeping of full folder tree (see examples below).

Installation
----------------
1) Unpack TreeCopyPlus.wcx in the folder with plugins.
2) In Total Commander go in "Configuration->Options->Packer->Configure packer extension WCXs".
3) Enter any unique extension (for example TreeCopyPlus). Press 'New type...' and select TreeCopyPlus.wcx.

Use
----------------
The plugin works: from the standard View, Branch View (Ctrl+B), results of search (Feed to listbox).
In one panel choose the files/folders necessary to you, in other - folder where it is necessary to copy these files with preservation of structure.
Press "alt+f5", choose TreeCopyPlus from the dropping list. If "Move to archive" is checked, files will be moved.
You can configure "Number of upper levels to exclude" - amount of
nestings from a root up to the CURRENT folder (in search results - up to the tree end, see example below) which are necessary to exclude (by default = 1).

Examples and explanations
----------------
The folder for copying:
"C:\Program Files\totalcmd\plugins\"
We copy in "D:\pusto\"

At "Number of upper levels to exclude" = 0, all files and subdirectories from "C:\Program Files\totalcmd\plugins\" are copied in:
"D:\pusto\C\Program Files\totalcmd\plugins\"
At "Number of upper levels to exclude" = 1, we shall receive:
"D:\pusto\Program Files\totalcmd\plugins\"
At "Number of upper levels to exclude" = 2, we shall receive:
"D:\pusto\totalcmd\plugins\"
etc... If in the given example to choose "Number of upper levels to exclude" >= 4 that plugin will give out the message:
"Number of exclusions are too big. Number of nesting dir = 3", nothing will be copied.

IMPORTANT!!!
If copying occurs from results of search, then "Number of upper levels to exclude" applied to each file and folder (from root to end).
Since, in this case the concept of the current folder is not determined. I.e. files for which given parameter
exceeds amount of nestings will not be copied! An example:
Results of search:
"C:\Program Files\totalcmd\plugins\"
"D:\win\UTIL\wincmd\PLUGINS\"
"D:\win\UTIL\plugin.txt"

"Number of upper levels to exclude" = 4, we copy in " D:\pusto\", result:
"D:\pusto\contents of folder "C:\Program Files\totalcmd\plugins\""
"D:\pusto\PLUGINS\"

Version history
----------------
1.051 - 26.09.11
+ Unicode support.
+ 64 bit version.
* Some corrections and improvements.

1.041 - 06.10.05
- Fixed bug with coping/move files from network (thanks Hynek Cernoch).

1.04 - 24.06.05
+ Value of "Number of upper levels to exclude" now saving in TreeCopyPlus.ini.

1.031 - 18.11.04
+ pluginst.inf file added for TC 6.5+
* Cosmetic code improvements

1.03 - 05.11.04
+ Dialogues for overwriting and deleting of Read-Only files.
* Correct processing of Read-Only files.
* Some improvements.

1.01 - 04.11.04
+ At concurrence of names, now it is possible to choose "Overwrite all" and "Skip all".

1.0 - 01.11.04
First release.

----------------
Wishes and found bugs: sukerrussia@mail.ru