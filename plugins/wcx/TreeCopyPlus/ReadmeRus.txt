TreeCopyPlus 1.051
(c) ��������� ������� ��� SUKER
----------------
Packer ������ ��� Total Commander. ������������ ��� �����������/����������� ������ � ������ ����������� ������ ��������� (��. ������� ����).

�����������
----------------
1) ���������� TreeCopyPlus.wcx � ������� � ���������.
2) � Total Commander ������� � "Configuration->Options->Packer->Configure packer extension WCXs".
3) ������� ����� ���������� ���������� (�������� TreeCopyPlus). ������� 'New type...' � �������� TreeCopyPlus.wcx.

�������������
----------------
������ ��������: �� ������� ������, ������ Branch View (Ctrl+B), ����������� ������ (Feed to listbox).
� ����� ������ ��������� ������ ��� �����/��������, � ������ ������� ���� ����� ����������� ��� ����� � ����������� ���������.
����� "alt+f5", ��������� TreeCopyPlus �� ����������� ������. ���� �������� "Move to archive", �� ����� ����� ����������.
����� �� ������ "Configure" �������� � ������ �������� � ����� ������ ;) "Number of upper levels to exclude" - ����������
�������� �� ����� �� ������� ����� (� ����������� ������ - �� ����� ������, �������� ������ ����), ������� ���������� ���������� (�� ��������� = 1).

������� � ���������
----------------
������� ��� �����������:
"C:\Program Files\totalcmd\plugins\"
�������� � "D:\pusto\"

��� "Number of upper levels to exclude" = 0, ��� ����� � ����������� "C:\Program Files\totalcmd\plugins\" ���������� �:
"D:\pusto\C\Program Files\totalcmd\plugins\"
��� "Number of upper levels to exclude" = 1, �������:
"D:\pusto\Program Files\totalcmd\plugins\"
��� "Number of upper levels to exclude" = 2, �������:
"D:\pusto\totalcmd\plugins\"
� �.�... ���� � ������ ������� ������� "Number of upper levels to exclude" >= 4 �� ������ ������ ���������:
"Number of exclusions are too big. Number of nesting dir = 3", ������ ����������� �� �����.

�����!!!
���� ����������� ���������� �� ����������� ������, �� "Number of upper levels to exclude" ����������� � ������� ����� � �������� (�� ����� �� ����� ������).
�.�. � ������ ������ ������� ������� ����� �� ����������. �.�. ����� ��� ������� ������ ��������
��������� ���������� �������� �� ����� �����������! ������:
���������� ������:
"C:\Program Files\totalcmd\plugins\"
"D:\win\UTIL\wincmd\PLUGINS\"
"D:\win\UTIL\plugin.txt"

"Number of upper levels to exclude" = 4, �������� � "D:\pusto\", ���������:
"D:\pusto\���������� ����� "C:\Program Files\totalcmd\plugins\""
"D:\pusto\PLUGINS\"

������� ������
----------------
1.051 - 26.09.11
+ ��������� �������.
+ 64 ������ ������.
* ��������� ����������� � ���������.

1.041 - 06.10.05
- ��������� ��� � ������������/������������ ������ �� ���� (������� Hynek Cernoch).

1.04 - 24.06.05
+ �������� "Number of upper levels to exclude" ������ ����������� � TreeCopyPlus.ini.

1.031 - 18.11.04
+ pluginst.inf �������� ��� TC 6.5+
* ������������� ��������� ����

1.03 - 05.11.04
+ ��������� ������� ��� ��������������� � �������� Read-Only ������.
* ���������� ��������� Read-Only ������.
* ��������� ���������.

1.01 - 04.11.04
+ ��� ���������� ����, ������ ����� ������� "Overwrite all" � "Skip all".

1.0 - 01.11.04
������ �����.

----------------
��������� � ��������� ���� ������� ��������� �� sukerrussia@mail.ru