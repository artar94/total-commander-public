MultiArc 1.4.1.7+ MVV Build
---------------------------

1. What is MultiArc?

MultiArc is an archiver plug-in for Total Commander. It allows viewing, extracting and modifying contents of archives that are unsupported by Total Commander (e.g. JAR, IMP). MultiArc translates the commands from Total Commander into corresponding external archiver calls. You can easily add support for your favorite archiver by editing MultiArc.ini.

Please refer to the online help for details about installing and configuring MultiArc.


2. MVV Build

Official MultiArc developing has been discontinued since 2007. However there was a need for 64-bit version when 64-bit Total Commander was released.
This build continues official MultiArc 1.4.1.7 version taken from its repository.
It comes with new MultiArc.chm help file. Some topics were updated according to introduced changes. Original unchanged help file MultiArc.hlp is shipped with this package (note that this format is deprecated since Windows Vista).


3. Links

Discussion page on official Total Commander board: http://ghisler.ch/board/viewtopic.php?t=16534
Discussion page on official Russian Total Commander board: http://forum.wincmd.ru/viewtopic.php?t=78
MultiArc Repository page (outdated): http://sourceforge.net/projects/wcx/files/MultiArc/

� 2007-2020 MVV
� 2005-2007 Vladimir Serdyuk
� 2000-2003 Siarzhuk Zharski



History:

2020-05-15	1.4.3.162
	* fixed ExcludeIDs support (feature was completely broken)

2017-10-13	1.4.3.160
	+ $MULTIARC pseudo-variable support in Archiver addon field
	* fixed safe %F expansion (first file was passed instead of all other files)

2017-02-19	1.4.2.158
	+ ignoring spaces in packed/unpacked sizes in listings (e.g. for NanoZip)
	* P modifier for %-variables now produces path with drive letter
	* safe %F/%L expansion (support % characters in path; no command line length limit)

2016-10-24	1.4.2.150
	+ scrollable edit field in %S parameter ask dialog
	* plugin should work under Win2K (some WinXP imports removed)
	* INI buffer size reduced to 65528 bytes (source code compatibility with Win98)

2016-07-29	1.4.2.144
	+ 'Edit MultiArc.ini...' button supports unquoted editor path w/o arguments
	* associated extensions detection in configuration dialog of 64-bit version
	* setting proper size for strings from INI on reading (it was always 64 KB)
	* better quoting INI strings on saving
	* properly save <SeekID> value to IDPos INI keys

2016-06-04	1.4.2.134
	* now MultiArc lists files only when necessary, not on every archive opening
	* better handling of invalid dates (reporting four-digit years < 1980 as 1980 and not as 20xx)

2016-05-31	1.4.2.122
	* fixed extracting from archive subfolder to a folder with same name (bye, buggy SHFileOperation function)

2015-10-19	1.4.1.114
	+ added Russian language file
	* zha*.tmp moved to $mltwcx
	* do not keep temp file open between archiver calls
	* close archive call didn't release resources sometimes
	* importing bad addon file could cause crash
	* memory leak in Language dialog page
	* %S option bug due to bad format string (since 1.4.1.101)
	* some translation improvements

2015-10-01	1.4.1.101
	* help and localization modifications

2015-09-20	1.4.1.96
	+ now CHM help is used
	+ 'Edit MultiArc.ini...' button supports parameters and %1 placeholder in TC editor setting
	+ MultiArc.ini edit window doesn't close when editor closes instantly, Done button added to it
	* fixed string truncation in wait for MultiArc.ini edit window
	* About dialog now shows numeric file version

2014-11-21	1.4.1.18
	! MVV Build #8
	* fixed batch unpack 'file not found' error (caused by generating temp folder name via creating temp file)
	* fixed errors on enter archive caused by locking pipe file by some antivirus software

2014-03-08	1.4.1.17
	! MVV Build #7
	* fixed UHARC support (broken since 1.4.1.14)
	* fixed crash on copy/view file (revealed in 1.4.1.14)
	* fixed crash on About dialog page (broken since 1.4.1.14)
	* working folders are now always created under %TEMP%\$mltwcx instead of just %TEMP%
	* fixed some batch unpack floating bugs

2014-03-07	1.4.1.16
	! MVV Build #6
	+ SkipSfxHeader now works and supports 64-bit stubs
	* SkipSfxHeader and SeekID support huge SFX archives
	* ExcludeIDs now checked for all matched formats, not only for first one
	* no more strange text parts displayed if packer output doesn't contain Start string (packer error)

2014-03-06	1.4.1.15
	! MVV Build #5
	* fixed wrong unpack directory bug (working directory is now passed to CreateProcess function instead of changing TC current directory)

2013-08-04	1.4.1.14
	! MVV Build #4
	* fixed buffer overflow bug when format string is longer than archiver output line

2013-06-25	1.4.1.12
	! MVV Build #2
	+ support for file sizes greater than 4 GB (WCX API ReadHeaderEx function)

2013-06-24	1.4.1.11
	! MVV Build #1
	+ 64-bit version of plugin added (requires 64-bit TC version)
	+ added + and ++ postfixes for format fields
	* some memory leaks fixed

2007-07-29	1.4.1.7
	! last official MultiArc release (older history is in MultiArc.hlp file)
